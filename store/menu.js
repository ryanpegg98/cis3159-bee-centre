// States
export const state = () => ({
  openMenu: false
})

// mutations
export const mutations = {

  TOGGLE_MENU (state) {
    state.openMenu = !state.openMenu
    state.openUserDetails = false
  },

  CLOSE_MENU (state) {
    state.openMenu = false
  }
}

// actions
export const actions = {

  toggleMenu ({ commit }) {
    commit('TOGGLE_MENU')
  },

  closeMenu ({ commit }) {
    commit('CLOSE_MENU')
  }
}

// Getters
export const getters = {
  menuStatus: state => state.openMenu
}
