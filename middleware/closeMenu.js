/* This middleware will close the menu when going to a new page */

export default function ({ store }) {
  store.dispatch('menu/closeMenu')
}
